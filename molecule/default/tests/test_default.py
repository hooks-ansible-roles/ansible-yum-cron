import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_yumcron(host):
    service = host.service("yum-cron")
    assert service.is_running
    assert service.is_enabled


def test_unit_file(host):
    file = host.file("/etc/yum/yum-cron.conf")
    assert file.exists
